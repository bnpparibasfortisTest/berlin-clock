//
//  LampState.swift
//  Berlin Clock
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import Foundation

struct LampStates
{
    var seconds: [String] = Array( repeating: "O", count: 1 )
    var singleMinutes: [String] = Array( repeating: "O", count: 4 )
    var fiveMinutes: [String] = Array( repeating: "O", count: 11 )
    var singleHours: [String] = Array( repeating: "O", count: 4 )
    var fiveHours: [String] = Array( repeating: "O", count: 4 )
    var clock: [String] = Array( repeating: "O", count: 24 )
}
