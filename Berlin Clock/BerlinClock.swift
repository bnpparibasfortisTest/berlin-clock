//
//  BerlinClock.swift
//  Berlin Clock
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import Foundation

class BerlinClock
{
    private let calendar: Calendar = Calendar.current
    
    func setLampStates(_ lampStates: LampStates, for date: Date ) -> LampStates
    {
        var newLampStates = lampStates
        
        let singleMinutes: SingleMinutes = SingleMinutes()
        newLampStates = singleMinutes.setLampStates( newLampStates, for: date )

        let fiveMinutes: FiveMinutes = FiveMinutes()
        newLampStates = fiveMinutes.setLampStates( newLampStates, for: date )
        
        let singleHours: SingleHours = SingleHours()
        newLampStates = singleHours.setLampStates( newLampStates, for: date )
        
        let fiveHours: FiveHours = FiveHours()
        newLampStates = fiveHours.setLampStates( newLampStates, for: date )
        
        let singleSeconds: SingleSeconds = SingleSeconds()
        newLampStates = singleSeconds.setLampStates( newLampStates, for: date )
        
        newLampStates.clock = newLampStates.seconds + newLampStates.fiveHours +  newLampStates.singleHours + newLampStates.fiveMinutes + newLampStates.singleMinutes
        
        return newLampStates
    }
    
    func digitalTime( from digitalTime: String ) -> String
    {
        return ""
    }
}
