//
//  SingleHours.swift
//  Berlin Clock
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import Foundation

class SingleHours
{
    private let calendar: Calendar = Calendar.current
    
    func setLampStates(_ lampStates: LampStates, for date: Date ) -> LampStates
    {
        var newLampStates = lampStates
        
        let hours: Int = calendar.component( .hour, from: date )
        let hoursOverFive: Int = hours % 5
        
        for (key, _) in newLampStates.singleHours.enumerated() {
            newLampStates.singleHours[key] = key < hoursOverFive ? "R" : "O"
        }
        
        return newLampStates
    }
}

