//
//  FiveMinutes.swift
//  Berlin Clock
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import Foundation

class FiveMinutes
{
    private let calendar: Calendar = Calendar.current
    private var state: String = "Y"
    
    func setLampStates(_ lampStates: LampStates, for date: Date ) -> LampStates
    {
        var newLampStates = lampStates
        let minutes: Int = calendar.component( .minute, from: date )
        let numberOfFiveMinutes: Int = minutes / 5
        
        for (key, _) in newLampStates.fiveMinutes.enumerated() {
            if key < numberOfFiveMinutes {
                state = (key+1) % 3 == 0 ? "R" : "Y"
            } else {
                state = "O"
            }
            
            newLampStates.fiveMinutes[key] = state
        }
        
        return newLampStates
    }
}
