//
//  FiveHours.swift
//  Berlin Clock
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import Foundation

class FiveHours
{
    private let calendar: Calendar = Calendar.current
    
    func setLampStates(_ lampStates: LampStates, for date: Date ) -> LampStates
    {
        var newLampStates = lampStates
        let hours: Int = calendar.component( .hour, from: date )
        let numberOfFiveHours: Int = hours / 5
        
        for (key, _) in newLampStates.fiveHours.enumerated() {
            newLampStates.fiveHours[key] = key < numberOfFiveHours ? "R" : "O"
        }
        return newLampStates
    }
}
