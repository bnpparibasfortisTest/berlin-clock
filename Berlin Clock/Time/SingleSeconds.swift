//
//  Seconds.swift
//  Berlin Clock
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import Foundation

class SingleSeconds
{
    private let calendar: Calendar = Calendar.current
    
    func setLampStates(_ lampStates: LampStates, for date: Date ) -> LampStates
    {
        var newLampStates = lampStates
        let seconds: Int = calendar.component( .second, from: date )
        newLampStates.seconds[0] = seconds % 2 == 0 ? "Y" : "O"

        return newLampStates
    }
}
