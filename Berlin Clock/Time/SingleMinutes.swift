//
//  Minutes.swift
//  Berlin Clock
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import Foundation

class SingleMinutes
{
    private let calendar: Calendar = Calendar.current
    
    func setLampStates(_ lampStates: LampStates, for date: Date ) -> LampStates
    {
        var newLampStates = lampStates
        
        let minutes: Int = calendar.component( .minute, from: date )
        let minutesOverFive: Int = minutes % 5
        
        for (key, _) in newLampStates.singleMinutes.enumerated() {
            newLampStates.singleMinutes[key] = key < minutesOverFive ? "Y" : "O"
        }
        
        return newLampStates
    }
}
