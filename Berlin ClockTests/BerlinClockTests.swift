//
//  ClockTests.swift
//  Berlin ClockTests
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import XCTest
@testable import Berlin_Clock

class BerlinClockTests: XCTestCase
{
    let cal: Calendar = Calendar.current
    let date: Date = Date()
    var lampStates: LampStates!
    
    override func setUp()
    {
        super.setUp()
        lampStates = LampStates()
    }

    func test_givenMidnight()
    {
        let testingDate: Date = cal.date( bySettingHour: 0, minute: 0, second: 0, of: date )!
        let berlinClock: BerlinClock = BerlinClock()
        let newLampStates: LampStates = berlinClock.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "Y", newLampStates.clock[0] )
        XCTAssertEqual( "O", newLampStates.clock[1] )
        XCTAssertEqual( "O", newLampStates.clock[2] )
        XCTAssertEqual( "O", newLampStates.clock[3] )
        XCTAssertEqual( "O", newLampStates.clock[4] )
        XCTAssertEqual( "O", newLampStates.clock[5] )
        XCTAssertEqual( "O", newLampStates.clock[6] )
        XCTAssertEqual( "O", newLampStates.clock[7] )
        XCTAssertEqual( "O", newLampStates.clock[8] )
        XCTAssertEqual( "O", newLampStates.clock[9] )
        XCTAssertEqual( "O", newLampStates.clock[10] )
        XCTAssertEqual( "O", newLampStates.clock[11] )
        XCTAssertEqual( "O", newLampStates.clock[12] )
        XCTAssertEqual( "O", newLampStates.clock[13] )
        XCTAssertEqual( "O", newLampStates.clock[14] )
        XCTAssertEqual( "O", newLampStates.clock[15] )
        XCTAssertEqual( "O", newLampStates.clock[16] )
        XCTAssertEqual( "O", newLampStates.clock[17] )
        XCTAssertEqual( "O", newLampStates.clock[18] )
        XCTAssertEqual( "O", newLampStates.clock[19] )
        XCTAssertEqual( "O", newLampStates.clock[20] )
        XCTAssertEqual( "O", newLampStates.clock[21] )
        XCTAssertEqual( "O", newLampStates.clock[22] )
        XCTAssertEqual( "O", newLampStates.clock[23] )
    }
    
    func test_givenOneSecondToMidnight()
    {
        let testingDate: Date = cal.date( bySettingHour: 23, minute: 59, second: 59, of: date )!
        let berlinClock: BerlinClock = BerlinClock()
        let newLampStates: LampStates = berlinClock.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "O", newLampStates.clock[0] )
        XCTAssertEqual( "R", newLampStates.clock[1] )
        XCTAssertEqual( "R", newLampStates.clock[2] )
        XCTAssertEqual( "R", newLampStates.clock[3] )
        XCTAssertEqual( "R", newLampStates.clock[4] )
        XCTAssertEqual( "R", newLampStates.clock[5] )
        XCTAssertEqual( "R", newLampStates.clock[6] )
        XCTAssertEqual( "R", newLampStates.clock[7] )
        XCTAssertEqual( "O", newLampStates.clock[8] )
        XCTAssertEqual( "Y", newLampStates.clock[9] )
        XCTAssertEqual( "Y", newLampStates.clock[10] )
        XCTAssertEqual( "R", newLampStates.clock[11] )
        XCTAssertEqual( "Y", newLampStates.clock[12] )
        XCTAssertEqual( "Y", newLampStates.clock[13] )
        XCTAssertEqual( "R", newLampStates.clock[14] )
        XCTAssertEqual( "Y", newLampStates.clock[15] )
        XCTAssertEqual( "Y", newLampStates.clock[16] )
        XCTAssertEqual( "R", newLampStates.clock[17] )
        XCTAssertEqual( "Y", newLampStates.clock[18] )
        XCTAssertEqual( "Y", newLampStates.clock[19] )
        XCTAssertEqual( "Y", newLampStates.clock[20] )
        XCTAssertEqual( "Y", newLampStates.clock[21] )
        XCTAssertEqual( "Y", newLampStates.clock[22] )
        XCTAssertEqual( "Y", newLampStates.clock[23] )
    }
    
    func test_givenSixteenFiftyZeroSix()
    {
        let testingDate: Date = cal.date( bySettingHour: 16, minute: 50, second: 6, of: date )!
        let berlinClock: BerlinClock = BerlinClock()
        let newLampStates: LampStates = berlinClock.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "Y", newLampStates.clock[0] )
        XCTAssertEqual( "R", newLampStates.clock[1] )
        XCTAssertEqual( "R", newLampStates.clock[2] )
        XCTAssertEqual( "R", newLampStates.clock[3] )
        XCTAssertEqual( "O", newLampStates.clock[4] )
        XCTAssertEqual( "R", newLampStates.clock[5] )
        XCTAssertEqual( "O", newLampStates.clock[6] )
        XCTAssertEqual( "O", newLampStates.clock[7] )
        XCTAssertEqual( "O", newLampStates.clock[8] )
        XCTAssertEqual( "Y", newLampStates.clock[9] )
        XCTAssertEqual( "Y", newLampStates.clock[10] )
        XCTAssertEqual( "R", newLampStates.clock[11] )
        XCTAssertEqual( "Y", newLampStates.clock[12] )
        XCTAssertEqual( "Y", newLampStates.clock[13] )
        XCTAssertEqual( "R", newLampStates.clock[14] )
        XCTAssertEqual( "Y", newLampStates.clock[15] )
        XCTAssertEqual( "Y", newLampStates.clock[16] )
        XCTAssertEqual( "R", newLampStates.clock[17] )
        XCTAssertEqual( "Y", newLampStates.clock[18] )
        XCTAssertEqual( "O", newLampStates.clock[19] )
        XCTAssertEqual( "O", newLampStates.clock[20] )
        XCTAssertEqual( "O", newLampStates.clock[21] )
        XCTAssertEqual( "O", newLampStates.clock[22] )
        XCTAssertEqual( "O", newLampStates.clock[23] )
    }
    
    func test_givenElevenThirtySevenZeroOne()
    {
        let testingDate: Date = cal.date( bySettingHour: 11, minute: 37, second: 1, of: date )!
        let berlinClock: BerlinClock = BerlinClock()
        let newLampStates: LampStates = berlinClock.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "O", newLampStates.clock[0] )
        XCTAssertEqual( "R", newLampStates.clock[1] )
        XCTAssertEqual( "R", newLampStates.clock[2] )
        XCTAssertEqual( "O", newLampStates.clock[3] )
        XCTAssertEqual( "O", newLampStates.clock[4] )
        XCTAssertEqual( "R", newLampStates.clock[5] )
        XCTAssertEqual( "O", newLampStates.clock[6] )
        XCTAssertEqual( "O", newLampStates.clock[7] )
        XCTAssertEqual( "O", newLampStates.clock[8] )
        XCTAssertEqual( "Y", newLampStates.clock[9] )
        XCTAssertEqual( "Y", newLampStates.clock[10] )
        XCTAssertEqual( "R", newLampStates.clock[11] )
        XCTAssertEqual( "Y", newLampStates.clock[12] )
        XCTAssertEqual( "Y", newLampStates.clock[13] )
        XCTAssertEqual( "R", newLampStates.clock[14] )
        XCTAssertEqual( "Y", newLampStates.clock[15] )
        XCTAssertEqual( "O", newLampStates.clock[16] )
        XCTAssertEqual( "O", newLampStates.clock[17] )
        XCTAssertEqual( "O", newLampStates.clock[18] )
        XCTAssertEqual( "O", newLampStates.clock[19] )
        XCTAssertEqual( "Y", newLampStates.clock[20] )
        XCTAssertEqual( "Y", newLampStates.clock[21] )
        XCTAssertEqual( "O", newLampStates.clock[22] )
        XCTAssertEqual( "O", newLampStates.clock[23] )
    }
}
