//
//  SingleMinutesTests.swift
//  Berlin ClockTests
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import XCTest
@testable import Berlin_Clock

class SingleMinutesTests: XCTestCase
{
    let cal: Calendar = Calendar.current
    let date: Date = Date()
    var lampStates: LampStates!
    
    override func setUp()
    {
        super.setUp()
        lampStates = LampStates()
    }
    
    func test_givenMidnight()
    {
        let testingDate: Date = cal.date( bySettingHour: 0, minute: 0, second: 0, of: date )!
        let singleMinutes: SingleMinutes = SingleMinutes()
        let newLampStates: LampStates = singleMinutes.setLampStates( lampStates, for: testingDate  )
        
        XCTAssertEqual( "O", newLampStates.singleMinutes[0] )
        XCTAssertEqual( "O", newLampStates.singleMinutes[1] )
        XCTAssertEqual( "O", newLampStates.singleMinutes[2] )
        XCTAssertEqual( "O", newLampStates.singleMinutes[3] )
    }
    
    func test_givenOneSecondToMidnight()
    {
        let testingDate: Date = cal.date( bySettingHour: 23, minute: 59, second: 59, of: date )!
        let singleMinutes: SingleMinutes = SingleMinutes()
        let newLampStates: LampStates = singleMinutes.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "Y", newLampStates.singleMinutes[0] )
        XCTAssertEqual( "Y", newLampStates.singleMinutes[1] )
        XCTAssertEqual( "Y", newLampStates.singleMinutes[2] )
        XCTAssertEqual( "Y", newLampStates.singleMinutes[3] )
    }
    
    func test_givenTwelveThirtyTwo()
    {
        let testingDate: Date = cal.date( bySettingHour: 12, minute: 32, second: 00, of: date )!
        let singleMinutes: SingleMinutes = SingleMinutes()
        let newLampStates: LampStates = singleMinutes.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "Y", newLampStates.singleMinutes[0] )
        XCTAssertEqual( "Y", newLampStates.singleMinutes[1] )
        XCTAssertEqual( "O", newLampStates.singleMinutes[2] )
        XCTAssertEqual( "O", newLampStates.singleMinutes[3] )
    }
    
    func test_givenTwelveThirtyFour()
    {
        let testingDate: Date = cal.date( bySettingHour: 12, minute: 34, second: 00, of: date )!
        let singleMinutes: SingleMinutes = SingleMinutes()
        let newLampStates: LampStates = singleMinutes.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "Y", newLampStates.singleMinutes[0] )
        XCTAssertEqual( "Y", newLampStates.singleMinutes[1] )
        XCTAssertEqual( "Y", newLampStates.singleMinutes[2] )
        XCTAssertEqual( "Y", newLampStates.singleMinutes[3] )
    }
    
    func test_givenTwelveThirtyFive()
    {
        let testingDate: Date = cal.date( bySettingHour: 12, minute: 35, second: 00, of: date )!
        let singleMinutes: SingleMinutes = SingleMinutes()
        let newLampStates: LampStates = singleMinutes.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "O", newLampStates.singleMinutes[0] )
        XCTAssertEqual( "O", newLampStates.singleMinutes[1] )
        XCTAssertEqual( "O", newLampStates.singleMinutes[2] )
        XCTAssertEqual( "O", newLampStates.singleMinutes[3] )
    }
}
