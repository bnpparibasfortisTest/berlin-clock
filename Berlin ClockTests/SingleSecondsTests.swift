//
//  SecondsTests.swift
//  Berlin ClockTests
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import XCTest
@testable import Berlin_Clock

class SingleSecondsTests: XCTestCase
{
    let cal: Calendar = Calendar.current
    let date: Date = Date()
    var lampStates: LampStates!
    
    override func setUp()
    {
        super.setUp()
        lampStates = LampStates()
    }
    
    func test_givenMidnight()
    {
        let testingDate: Date = cal.date( bySettingHour: 0, minute: 0, second: 0, of: date )!
        let singleSeconds: SingleSeconds = SingleSeconds()
        let newLampStates: LampStates = singleSeconds.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "Y", newLampStates.seconds[0] )
    }
    
    func test_givenOneSecondToMidnight()
    {
        let testingDate: Date = cal.date( bySettingHour: 23, minute: 59, second: 59, of: date )!
        let singleSeconds: SingleSeconds = SingleSeconds()
        let newLampStates: LampStates = singleSeconds.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "O", newLampStates.seconds[0] )
    }
}
