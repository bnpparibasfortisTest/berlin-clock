//
//  DigitalTimeTests.swift
//  Berlin ClockTests
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import XCTest
@testable import Berlin_Clock

class DigitalTimeTests: XCTestCase
{
    func test_givenMidnight()
    {
        let berlinClock: BerlinClock = BerlinClock()
        let date: String = berlinClock.digitalTime( from: "YOOOOOOOOOOOOOOOOOOOOOOO" )

        XCTAssertEqual( date, "00:00:00" )
    }
    
    func test_givenOneSecondToMidnight()
    {
        let berlinClock: BerlinClock = BerlinClock()
        let date: String = berlinClock.digitalTime( from: "ORRRRRRROYYRYYRYYRYYYYYY" )
        
        XCTAssertEqual( date, "23:59:59" )
    }
 
    func test_givenSixteenFiftyZeroSix()
    {
        let berlinClock: BerlinClock = BerlinClock()
        let date: String = berlinClock.digitalTime( from: "YRRROROOOYYRYYRYYRYOOOOO" )
        
        XCTAssertEqual( date, "16:50:06" )
    }
    
    func test_givenElevenThirtySevenZeroOne()
    {
        let berlinClock: BerlinClock = BerlinClock()
        let date: String = berlinClock.digitalTime( from: "ORROOROOOYYRYYRYOOOOYYOO" )
        
        XCTAssertEqual( date, "11:37:01" )
    }
}
