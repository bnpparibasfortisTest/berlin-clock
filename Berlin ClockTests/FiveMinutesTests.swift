//
//  FiveMinutesTests.swift
//  Berlin ClockTests
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import XCTest
@testable import Berlin_Clock

class FiveMinutesTests: XCTestCase
{
    let cal: Calendar = Calendar.current
    let date: Date = Date()
    var lampStates: LampStates!
    
    override func setUp()
    {
        super.setUp()
        lampStates = LampStates()
    }
    
    func test_givenMidnight()
    {
        let testingDate: Date = cal.date( bySettingHour: 0, minute: 0, second: 0, of: date )!
        let fiveMinutes: FiveMinutes = FiveMinutes()
        let newLampStates: LampStates = fiveMinutes.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "O", newLampStates.fiveMinutes[0] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[1] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[2] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[3] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[4] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[5] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[6] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[7] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[8] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[9] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[10] )
    }
    
    func test_givenOneSecondToMidnight()
    {
        let testingDate: Date = cal.date( bySettingHour: 23, minute: 59, second: 59, of: date )!
        let fiveMinutes: FiveMinutes = FiveMinutes()
        let newLampStates: LampStates = fiveMinutes.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[0] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[1] )
        XCTAssertEqual( "R", newLampStates.fiveMinutes[2] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[3] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[4] )
        XCTAssertEqual( "R", newLampStates.fiveMinutes[5] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[6] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[7] )
        XCTAssertEqual( "R", newLampStates.fiveMinutes[8] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[9] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[10] )
    }
    
    func test_givenTwelveZeroFour()
    {
        let testingDate: Date = cal.date( bySettingHour: 12, minute: 4, second: 0, of: date )!
        let fiveMinutes: FiveMinutes = FiveMinutes()
        let newLampStates: LampStates = fiveMinutes.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "O", newLampStates.fiveMinutes[0] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[1] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[2] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[3] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[4] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[5] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[6] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[7] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[8] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[9] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[10] )
    }
    
    func test_givenTwelveTwentyThree()
    {
        let testingDate: Date = cal.date( bySettingHour: 12, minute: 23, second: 0, of: date )!
        let fiveMinutes: FiveMinutes = FiveMinutes()
        let newLampStates: LampStates = fiveMinutes.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[0] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[1] )
        XCTAssertEqual( "R", newLampStates.fiveMinutes[2] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[3] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[4] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[5] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[6] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[7] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[8] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[9] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[10] )
    }
    
    func test_givenTwelveThirtyFive()
    {
        let testingDate: Date = cal.date( bySettingHour: 12, minute: 35, second: 0, of: date )!
        let fiveMinutes: FiveMinutes = FiveMinutes()
        let newLampStates: LampStates = fiveMinutes.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[0] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[1] )
        XCTAssertEqual( "R", newLampStates.fiveMinutes[2] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[3] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[4] )
        XCTAssertEqual( "R", newLampStates.fiveMinutes[5] )
        XCTAssertEqual( "Y", newLampStates.fiveMinutes[6] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[7] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[8] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[9] )
        XCTAssertEqual( "O", newLampStates.fiveMinutes[10] )
    }
}
