//
//  SingleHoursTests.swift
//  Berlin ClockTests
//
//  Created by Bobby on 20/03/2019.
//  Copyright © 2019 Bobby. All rights reserved.
//

import XCTest
@testable import Berlin_Clock

class SingleHoursTests: XCTestCase
{
    let cal: Calendar = Calendar.current
    let date: Date = Date()
    var lampStates: LampStates!
    
    override func setUp()
    {
        super.setUp()
        lampStates = LampStates()
    }
    
    func test_givenMidnight()
    {
        let testingDate: Date = cal.date( bySettingHour: 0, minute: 0, second: 0, of: date )!
        let singleHours: SingleHours = SingleHours()
        let newLampStates: LampStates = singleHours.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "O", newLampStates.singleHours[0] )
        XCTAssertEqual( "O", newLampStates.singleHours[1] )
        XCTAssertEqual( "O", newLampStates.singleHours[2] )
        XCTAssertEqual( "O", newLampStates.singleHours[3] )
    }
    
    func test_givenOneSecondToMidnight()
    {
        let testingDate: Date = cal.date( bySettingHour: 23, minute: 59, second: 59, of: date )!
        let singleHours: SingleHours = SingleHours()
        let newLampStates: LampStates = singleHours.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "R", newLampStates.singleHours[0] )
        XCTAssertEqual( "R", newLampStates.singleHours[1] )
        XCTAssertEqual( "R", newLampStates.singleHours[2] )
        XCTAssertEqual( "O", newLampStates.singleHours[3] )
    }
    
    func test_givenTwoZeroFour()
    {
        let testingDate: Date = cal.date( bySettingHour: 2, minute: 4, second: 0, of: date )!
        let singleHours: SingleHours = SingleHours()
        let newLampStates: LampStates = singleHours.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "R", newLampStates.singleHours[0] )
        XCTAssertEqual( "R", newLampStates.singleHours[1] )
        XCTAssertEqual( "O", newLampStates.singleHours[2] )
        XCTAssertEqual( "O", newLampStates.singleHours[3] )
    }
    
    func test_givenEightTwentyThree()
    {
        let testingDate: Date = cal.date( bySettingHour: 8, minute: 23, second: 0, of: date )!
        let singleHours: SingleHours = SingleHours()
        let newLampStates: LampStates = singleHours.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "R", newLampStates.singleHours[0] )
        XCTAssertEqual( "R", newLampStates.singleHours[1] )
        XCTAssertEqual( "R", newLampStates.singleHours[2] )
        XCTAssertEqual( "O", newLampStates.singleHours[3] )
    }
    
    func test_givenFourteenThirtyFive()
    {
        let testingDate: Date = cal.date( bySettingHour: 14, minute: 35, second: 0, of: date )!
        let singleHours: SingleHours = SingleHours()
        let newLampStates: LampStates = singleHours.setLampStates( lampStates, for: testingDate )
        
        XCTAssertEqual( "R", newLampStates.singleHours[0] )
        XCTAssertEqual( "R", newLampStates.singleHours[1] )
        XCTAssertEqual( "R", newLampStates.singleHours[2] )
        XCTAssertEqual( "R", newLampStates.singleHours[3] )
    }
}
